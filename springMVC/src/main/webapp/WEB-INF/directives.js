var datatableApp = angular.module('datatablesDirectives', [
    'ngRoute'
]);
datatableApp.directive('transactionDatatable', function($rootScope, $compile, ErrorMsgSvc, DataStorageSvc) {
    return {
        restrict : 'E',
        scope : {
            tableId : '@',
            enabledColumns : '=',
            // tableDataSet : '=',
            emptyMessage : '@',
            showEmptyMessage : '=',
            rowCallbackFunction : '&',
            headerClickSort : '&',
            tableScrollHeight : '@',
            initialSort : '=',
            initialDataSet : '=',
            summaryTable : '@'
        },
        controller : 'TransactionDatatableCtrl',
        templateUrl : function($node, attrs) {
            return 'views/datatables/' + attrs.tableTemplate;
        },
        replace : true,
        link : function(scope, elem, attrs) {
            var options = {
                "bAutoWidth" : true,
                "sScrollX" : "100%",
                "bProcessing" : true,
                "aoColumns" : [],
                "aaSorting" : [],
                "aoColumnDefs" : [],
                'oLanguage' : {
                    sEmptyTable : ""
                }
            };
            // If dtoptions is not declared, check the other options
            if (attrs['dtoptions'] === undefined) {
                for (property in attrs) {
                    switch (property) {
                        // This is the ajax source
                        case 'sajaxsource':
                            options['sAjaxSource'] = attrs[property];
                            break;
                        case 'sajaxdataprop':
                            options['sAjaxDataProp'] = attrs[property];
                            break;
                    }
                }
            } else {
                // If dtoptions is declare, extend the current options with it.
                angular.extend(options, $scope[attrs['dtoptions']]);
            }

            _.each(elem.find('thead th'), function(v, k, l) {
                var data = angular.element(v).data();
                var option = {
                    'aTargets' : [
                        k
                    ]
                };
                if (data.sortable !== undefined) {
                    option.bSortable = data.sortable;
                }
                if (data.sorttype !== undefined) {
                    option.sType = data.sorttype;
                }
                if (data.datatype !== undefined && aofs_datatypes[data.datatype] !== undefined) {
                    datatype = aofs_datatypes[data.datatype];
                    option.mRender = datatype.render;
                    option.sType = datatype.sortType;
                }
                options.aoColumnDefs.push(option);

                options.aoColumns.push({
                    mData : data.mdata,
                    sClass : data.sclass,
                    sWidth : data.swidth,
                    bVisible : scope.enabledColumns.contains(data.mdata)
                });

                if (data.sortable == true) {
                    $(v).click(function($event) {
                        scope.headerClickSort({
                            eventObj : $event
                        });
                    })
                }

            });

            if (scope.initialSort !== undefined) { // TODO: Left room for multisort, but not tested
                for (var i = 0; i < scope.initialSort.length; i++) {
                    var sortParams = scope.initialSort[i].split(":");
                    var headers = $(elem).find('thead th');
                    for (var j = 0; j < headers.length; j++) {
                        if ($(headers[j]).attr('data-mdata') == sortParams[0]) {
                            options.aaSorting.push([
                                j,
                                sortParams[1] == undefined ? "asc" : sortParams[1]
                            ]);
                            break;
                        }
                    }
                }
                ;
            }

            if (scope.tableScrollHeight) {
                options.sScrollY = scope.$eval(scope.tableScrollHeight);
            }

            options.showEmptyMessage = scope.showEmptyMessage ? scope.showEmptyMessage : false;
            options.oLanguage.sEmptyTable = scope.emptyMessage ? ErrorMsgSvc.getMessage(scope.emptyMessage) : ErrorMsgSvc
                .getMessage("NO_RESULTS_FOUND");

            options.fnCreatedRow = function(row, data) {
                if (data['statusClass'] !== undefined) {
                    $(row).addClass(data['statusClass'].replace("&nbsp;", "").toLowerCase());
                }
            };

            // Load the datatable!
            scope.dataTable = $("#" + scope.tableId).dataTable(options);

            function createResizeWidthHandler(dt) {
                var width = -1;
                return function(event) {
                    if ($(window).width() !== width) {
                        width = $(window).width();
                        dt.fnAdjustColumnSizing();
                    }
                };
            }
            $(window).on('resize', createResizeWidthHandler(scope.dataTable));

            $(scope.dataTable).on("click", "tr", function(event) {
                scope.rowCallbackFunction({
                    row : event.currentTarget,
                    datatable : scope.dataTable
                });
            });

            if (scope.initialDataSet !== undefined) {
                console.log(scope.initialDataSet); // DEBUG
                scope.dataTable.fnAddData(scope.initialDataSet);
            }

            // // Can be enabled to watch outside service resource for data
            // if (tableDataSet !== undefined) {
            // scope.$watch(function() {
            // return DataStorageSvc.get(scope.tableDataSet);
            // }, function(data) {
            // var newTableState = DataStorageSvc.get(scope.tableDataSet);
            // if (newTableState) {
            // if (newTableState.data) {
            // scope.dataTable.fnAddData(newTableState.data);
            // }
            // }
            // if (scope.summaryTable !== undefined) {
            // $("#" + scope.summaryTable).width($(scope.dataTable).width() - 1); // align summary header with table width
            // }
            // scope.$emit(scope.tableId + '_loaded');
            // scope.dataTable.fnDraw(true);
            // });
            // }

            // // Can be enabled for multiple select functionality
            // scope.dataTable.on('click', '.blockTblcb', function() { // TODO Need to make this an $emit event
            // if ($('.blockTblcb:checked').length > 0) {
            // $('#rejectTradesBtn').removeAttr('disabled');
            // $('#linkTradeBtn').removeAttr('disabled');
            // } else {
            // $('#rejectTradesBtn').attr('disabled', 'disabled');
            // $('#linkTradeBtn').attr('disabled', 'disabled');
            // }
            // });

            // check all header checkbox // Can be enabled for multiple select functionality
            // angular.element('#sAllAccTbl').die('change'); // clear any old listeners
            // angular.element('#sAllAccTbl').live('change', function() {
            // if (angular.element(this).is(':checked')) {
            // $(scope.dataTable._('tr', {
            // "filter" : "applied"
            // })).each(function(index) {
            // $("input#cb_" + this.tradeID + "").attr('checked', true);
            // });
            // } else {
            // angular.element('input', scope.dataTable.fnGetNodes()).attr('checked', false);
            // }
            // $('#linkTradeBtn').attr('disabled', 'disabled');
            // $('#rejectTradesBtn').attr('disabled', 'disabled');
            // });

            scope.$on('$destroy', function() {
                $(scope.dataTable).off("click", "tr");
            });
        }
    };
});

datatableApp.directive('modalDatatable', function($rootScope, ErrorMsgSvc) {
    return {
        restrict : 'A',
        link : function(scope, elem, attrs) {
            var options = {
                "bProcessing" : true,
                "aoColumns" : [],
                "aaSorting" : [],
                "aoColumnDefs" : [],
                "sScrollX" : "100%",
                "oLanguage" : {
                    "sZeroRecords" : "No trades to display"
                }
            };
            _.each(elem.find('thead th'), function(v, k, l) {
                var data = angular.element(v).data();
                var option = {
                    'aTargets' : [
                        k
                    ]
                };
                if (data.sortable !== undefined) {
                    option.bSortable = data.sortable;
                }
                if (data.sorttype !== undefined) {
                    option.sType = data.sorttype;
                }
                if (data.datatype !== undefined && aofs_datatypes[data.datatype] !== undefined) {
                    datatype = aofs_datatypes[data.datatype];
                    option.mRender = datatype.render;
                    option.sType = datatype.sortType;
                }
                options.aoColumnDefs.push(option);

                options.aoColumns.push({
                    mData : data.mdata,
                    sClass : data.sclass,
                    sWidth : data.swidth,
                    bVisible : data.bvisible
                });

                options.fnCreatedRow = function(row, data) {

                };

                if (attrs.initialSort !== undefined && attrs.initialSort === data.mdata) {
                    options.aaSorting.push([
                        k,
                        attrs.initialSortDirection === undefined ? "asc" : attrs.initialSortDirection
                    ]);
                }
            });

            if (attrs.showEmptyMessage) {
                options.showEmptyMessage = true;
                options.oLanguage.sEmptyTable = ErrorMsgSvc.getMessage(attrs.showEmptyMessage);
            }

            if (attrs.scrollHeight) {
                options.sScrollY = scope.$eval(attrs.scrollHeight);
            }

            // Load the datatable!
            var modalTable = elem.dataTable(options);

            if (attrs.aaData !== undefined) {
                scope.$watch(attrs.aaData, function(data) {
                    if (data) {
                        modalTable.fnClearTable();
                        modalTable.fnAddData(data, true);
                        modalTable.find("input.rb_select").on('click', function() {
                            scope.$emit('rbSelected');
                        })

                        // THIS CODE NEEDED TO BE REFACTORED AND MOVED TO trade.directive.js
                        if (scope.AccTableSelectedRowId !== undefined && scope.AccTableSelectedRowId !== false) {
                            var row = $("#" + scope.AccTableSelectedRowId + "");
                            if ($(row).hasClass('selected') == false)
                                angular.element("#" + scope.AccTableSelectedRowId + "").addClass("selected");
                            scope.detailsRows = scope.tradeTableData[scope.AccTableSelectedRowId];
                            scope.moreinfoIsCollapsed = false;
                        }
                    }
                    if (attrs.summaryTable) {
                        $("#" + attrs.summaryTable).width($(modalTable).width() - 1); // align summary header since table can shift with new data.
                    }
                });
            }

            if (attrs.fnRowCallback) {
                var rowCallback = scope.$eval(attrs.fnRowCallback);
                modalTable.on("click", "tr", function(event) {
                    rowCallback(event.currentTarget);
                });
            }
        }
    };
});

datatableApp.directive('userDefinedDatatable', function($rootScope, $timeout, ErrorMsgSvc) {
    return {
        restrict : 'E',
        scope : {
            tableStructure : '=',
            tableData : '=',
            id : '@'
        },
        template : "<table id={{id}} class='table' cellspacing='0' cellpadding='0'>" + "<thead><tr>"
            + "<th ng-repeat='column in tableStructure.columns' sortable='{{column.sortable}}'>{{column.name}}</tr>" + "</table>",
        link : function(scope, elem, attrs) {
            $timeout(function() {
                var options = {
                    "aaData" : scope.tableData,
                    "bProcessing" : true,
                    "aoColumns" : [],
                    "aaSorting" : [],
                    "aoColumnDefs" : [],
                    "sScrollX" : "100%",
                    "oLanguage" : {
                        "sZeroRecords" : "No trades to display"
                    }
                };
                _.each(elem.find('thead th'), function(v, k, l) {
                    var data = angular.element(v).data().$scope.column;
                    var option = {
                        'aTargets' : [
                            k
                        ]
                    };
                    if (data.sortable !== undefined) {
                        option.bSortable = data.sortable;
                    }
                    if (data.sorttype !== undefined) {
                        option.sType = data.sorttype;
                    }
                    if (data.datatype !== undefined && aofs_datatypes[data.datatype] !== undefined) {
                        datatype = aofs_datatypes[data.datatype];
                        option.mRender = datatype.render;
                        option.sType = datatype.sortType;
                    }
                    options.aoColumnDefs.push(option);

                    options.aoColumns.push({
                        mData : data.name
                    });
                });
                $(elem).find("table").dataTable(options);
            });
        }
    };
});